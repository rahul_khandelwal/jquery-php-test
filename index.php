<html lang="en">
<head>
	<title>Jquery Php Test</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script type="text/javascript" src="index.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<style type="text/css">
		input:invalid {
			color: red;
		}	
	</style>
</head>
<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">Discount(1.Bebas:6% 2.Bebas 2D:10% 3.Naga:10%)</div>
			<div class="panel-body">
				<form action="action.php" id="calcuationForm" method="post">
					<div class="input-group control-group after-add-more">
						<table class="table" width="100%">
							<thead><tr><!-- <th>No.</th> --><th>Tebak Bebas/Bebas 2D/Naga</th><th>COLOK Bebas</th><th>COLOK Bebas 2D</th><th>COLOK Naga</th></tr>
								<tr>
									<!-- <td>1</td> -->
									<td>
										<table>
											<tr>
												<td>
													<input type="text" name="no_1[]" class="form-control input-sm numeric inputs" maxlength="1" pattern="^[0-9]$">
												</td>
												<td>
													<input type="text" name="no_2[]" class="form-control input-sm numeric inputs" maxlength="1" pattern="^[0-9]$">
												</td>
												<td>
													<input type="text" name="no_3[]" class="form-control input-sm numeric inputs" maxlength="1" pattern="^[0-9]$">
												</td>
											</tr>
										</table>
									</td>
									<td>
										<table>
											<tr>
												<td colspan="2">
													<input type="text" name="amount1[]" class="form-control input-sm bet-amount inputs" placeholder="Bet Amount" pattern="[0-9]+(\.[0-9]{0,2})?" required>
												</td>
											</tr>
											<tr style="display: none">
												<td>
													<input type="text" name="discount1[]" placeholder="Discout" readonly="true" class="form-control input-sm">
												</td>
												<td>
													<input type="text" name="bayar1[]" placeholder="Bayar" class="form-control input-sm" readonly="true">
												</td>
												<td>
													<input type="hidden" name="disc_per1[]" value="6"></td>
												</tr>
											</table>
										</td>
										<td>
											<table>
												<tr>
													<td colspan="2">
														<input type="text" name="amount2[]" class="form-control input-sm bet-amount inputs" placeholder="Bet Amount" pattern="[0-9]+(\.[0-9]{0,2})?" required>
													</td>
												</tr>
												<tr style="display: none">
													<td>
														<input type="text" name="discount2[]" placeholder="Discout" readonly="true" class="form-control input-sm">
													</td>
													<td>
														<input type="text" name="bayar2[]" placeholder="Bayar" class="form-control input-sm" readonly="true">
													</td>
													<td>
														<input type="hidden" name="disc_per2[]" value="10"></td>
													</tr>
												</table>
											</td>
											<td>
												<table>
													<tr>
														<td colspan="2">
															<input type="text" name="amount3[]" class="form-control input-sm bet-amount inputs" placeholder="Bet Amount" pattern="[0-9]+(\.[0-9]{0,2})?" required>
														</td>
													</tr>
													<tr style="display: none">
														<td>
															<input type="text" name="discount3[]" placeholder="Discout" readonly="true" class="form-control input-sm">
														</td>
														<td>
															<input type="text" name="bayar3[]" placeholder="Bayar" class="form-control input-sm" readonly="true">
														</td>
														<td>
															<input type="hidden" name="disc_per3[]" value="10"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<div class="clear"></div>
									<div class="input-group-btn" align="right"> 
										<button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add More</button>
									</div>
									<div class="clear"></div>
									<div class="input-group-btn" align="center"> 						<button class="btn btn-success" type="submit">Submit</button>
										<button class="btn btn-danger" type="cancel">Cancel</button>
									</div>
								</form>
								<!-- Copy Fields -->
								<div class="copy hide">
									<div class="control-group input-group" style="margin-top:10px">
										<table>
											<tr>
												<!-- <td>1</td> -->
												<td>
													<table>
														<tr>
															<td>
																<input type="text" name="no_1[]" class="form-control input-sm numeric" maxlength="1" pattern="^[0-9]$">
															</td>
															<td>
																<input type="text" name="no_2[]" class="form-control input-sm numeric" maxlength="1" pattern="^[0-9]$">
															</td>
															<td>
																<input type="text" name="no_3[]" class="form-control input-sm numeric" maxlength="1" pattern="^[0-9]$">
															</td>
														</tr>
													</table>
												</td>
												<td>
													<table>
														<tr>
															<td colspan="2">
																<input type="text" name="amount1[]" class="form-control input-sm bet-amount" placeholder="Bet Amount" pattern="[0-9]+(\.[0-9]{0,2})?" required>
															</td>
														</tr>
														<tr style="display: none">
															<td>
																<input type="text" name="discount1[]" placeholder="Discout" readonly="true" class="form-control input-sm">
															</td>
															<td>
																<input type="text" name="bayar1[]" placeholder="Bayar" class="form-control input-sm" readonly="true">
															</td>
															<td>
																<input type="hidden" name="disc_per1[]" value="10"></td>
															</tr>
														</table>
													</td>
													<td>
														<table>
															<tr>
																<td colspan="2">
																	<input type="text" name="amount2[]" class="form-control input-sm bet-amount" placeholder="Bet Amount" pattern="[0-9]+(\.[0-9]{0,2})?" required>
																</td>
															</tr>
															<tr style="display: none">
																<td>
																	<input type="text" name="discount2[]" placeholder="Discout" readonly="true" class="form-control input-sm">
																</td>
																<td>
																	<input type="text" name="bayar2[]" placeholder="Bayar" class="form-control input-sm" readonly="true">
																</td>
																<td>
																	<input type="hidden" name="disc_per2[]" value="10"></td>
																</tr>
															</table>
														</td>
														<td>
															<table>
																<tr>
																	<td colspan="2">
																		<input type="text" name="amount3[]" class="form-control input-sm bet-amount" placeholder="Bet Amount" pattern="[0-9]+(\.[0-9]{0,2})?" required>
																	</td>
																</tr>
																<tr style="display: none">
																	<td>
																	<input type="text" name="discount3[]" placeholder="Discout" readonly="true" class="form-control input-sm">
																	</td>
																	<td>
																		<input type="text" name="bayar3[]" placeholder="Bayar" class="form-control input-sm" readonly="true">
																	</td>
																	<td>
																		<input type="hidden" name="disc_per3[]" value="10"></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<div class="input-group-btn"> 
														<button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
													</div>
												</div>
											</div>
											<!-- Display Output -->
											<div class="result" style="display: none;"></div>
										</div>
									</div>
								</div>
							</body>
							</html>