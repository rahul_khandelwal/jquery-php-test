$(document).ready(function() {
  /*var charLimit = 1;
  $(".inputs").keydown(function(e) {

    var keys = [8, 9,  19, 20, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 144, 145];

    if (e.which == 8 && this.value.length == 0) {
      $(this).prev('.inputs').focus();
    } else if ($.inArray(e.which, keys) >= 0) {
      return true;
    } else if (this.value.length >= charLimit) {
      $(this).next('.inputs').focus();
      return false;
    } else if (e.shiftKey || e.which <= 48 || e.which >= 58) {
      return false;
    }
  }).keyup (function () {
    if (this.value.length >= charLimit) {
      $(this).next('.inputs').focus();
      return false;
    }
  });*/
  $("body").on("change",".bet-amount", function() {
    var nextTr = $(this).closest('tr').next('tr');
    nextTr.show();
    var amount  = parseFloat($(this).val()).toFixed(2);
    var discountPer = parseFloat(nextTr.find("input[type=hidden]").val()).toFixed(2);
    var discount = parseFloat((amount*discountPer)/100).toFixed(2);
    var bayar = parseFloat(amount-discount).toFixed(2);
    nextTr.find("td:first-child input[type=text]").val(discount);
    nextTr.find("td:nth-child(2) input[type=text]").val(bayar);
  });

  $(".add-more").click(function(){ 
    var html = $(".copy").html();
    $(".after-add-more").after(html);
  });

  $("body").on("click",".remove",function(){ 
    $(this).parents(".control-group").remove();
  });

  var frm = $('#calcuationForm');
  frm.submit(function (e) {
    e.preventDefault();
    $.ajax({
      type: frm.attr('method'),
      url: frm.attr('action'),
      data: frm.serialize(),
      dataType: "json",
      success: function (data) {
        console.log('Submission was successful.');
        var totalPay=0;
        var html = '<table class="table"><thead><tr><th>No.</th><th>Name</th><th>TebaKan</th><th>Bet</th><th>Bet</th><th>Bayar</th><th>X-Menang</th></tr><tbody>';
        $.each(data, function (i) {
          totalPay+=parseFloat(data[i]['bayar']);
          html+= '<tr>';     
          $.each(data[i], function (key, val) {
            html+= '<td>'+val+'</td>';
          });
          html+='</tr>';
        });
        html+='<tr><td>Total Bet Player</td><td>'+parseFloat(totalPay).toFixed(2)+'</td></tr></tbody></table>';
        $('.result').html(html);
        $('.result').show();
        console.log(data);
      },
      error: function (data) {
        console.log('An error occurred.');
        console.log(data);
      },
    });
  });
});