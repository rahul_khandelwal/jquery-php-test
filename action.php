<?php
$nameArr = [1=>"Bebas", 2=>"Bebas 2D", 3=>"Naga"];
// As per example discount assume 10%
// I assume amount for single quantity
$resultArr = array();
$j=0;
for ($i=1 ; $i<=3; $i++) {
	foreach ($_POST["no_$i"] as $key => $value) {
		$amount = $_POST["amount$i"][$key];
		$bayar = $_POST["bayar$i"][$key];
		$resultArr[$j]['sr'] = $j+1;
		$resultArr[$j]["name"] = $nameArr[$i];
		if($i>1){
			$prevI = $i-1;
			$preNumber = $_POST["no_$prevI"][$key];
			$_POST["no_$i"][$key] = $preNumber."-".$value;
		}
		$resultArr[$j]["number"] = $_POST["no_$i"][$key];
		$resultArr[$j]["amount"] = $amount;
		$resultArr[$j]["disc_per"] = $_POST["disc_per$i"][$key];
		$resultArr[$j]["bayar"] = $bayar;
		if($i==1){
			$x_men_per = 1.5;
		}
		if($i==2){
			$x_men_per = 6;
		}
		if($i==3){
			$x_men_per = 17;
		}
		$cal = ($amount*$x_men_per)+$bayar;
		$resultArr[$j]["x-men"] = "X $x_men_per = ".$cal;
		$j++;
	}
}
print json_encode($resultArr);
?>